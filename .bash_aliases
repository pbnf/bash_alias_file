## topbuild aliases ##
alias tbb='topbuild build -v'
alias bbt='topbuild clean'
alias tbi='topbuild info'
alias tbr='topbuild refresh -v'
alias tb='topbuild'
alias ee='emea_repos_exec'
alias rr='top-exec'
alias setx86='topbuild toolchain padtec.fw.rsc.toolchain.gnu.x86_64.1'
alias seta5='topbuild toolchain padtec.fw.rsc.toolchain.padlinux.arm.a5.1'
alias setarm1='topbuild toolchain padtec.fw.rsc.toolchain.mentor.arm.1'

## unsorted ##
alias sublime='/opt/sublime_text_2/sublime_text'
alias lll='ls -ltr'
alias lsc='gnome-screensaver-command -l'
alias sl='ls'

## get rid of command not found ##
alias cd..='cd ..'
 
## a quick way to get out of current directory ##
alias cd..='cd ..'
alias ..='cd ..'
alias ..1='cd ..'
alias ..2='cd ../../'
alias ..3='cd ../../../'
alias ..4='cd ../../../../'
alias ..5='cd ../../../../..'
alias ..6='cd ../../../../../..'
alias ..7='cd ../../../../../../..'
alias ..8='cd ../../../../../../../..'
alias ..9='cd ../../../../../../../../..'
alias ..10='cd ../../../../../../../../../..'

## a5 strip bin
alias a5_strip='/opt/padlinux/amp/1.6.1.0/sysroots/x86_64-padlinuxsdk-linux/usr/libexec/arm-padtec-linux-gnueabi/gcc/arm-padtec-linux-gnueabi/4.8.2/strip -s'

## Colorize the grep command output for ease of use (good for log files)##
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

## install  colordiff package ##
alias diff='colordiff'